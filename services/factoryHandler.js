const catchAsync = require("../utils/catchAsync"),
   AppError = require("../utils/appError");

const docNotFound = (next) => {
   return next(new AppError("No doc is associated with this id", 404));
};

function sendResponse(doc, res) {
   res.status(200).json({
      status: "success",
      data: {
         data: doc,
      },
   });
}

exports.deleteOne = (Model) =>
   catchAsync(async (req, res, next) => {
      const doc = await Model.findByIdAndDelete(req.params.id);

      if (!doc) {
         return docNotFound(next);
      }
      sendResponse(doc, res);
   });

exports.updateOne = (Model) =>
   catchAsync(async (req, res, next) => {
      const doc = await Model.findByIdAndUpdate(req.params.id, req.body, {
         new: true,
         runValidators: true,
      });
      if (!doc) {
         return docNotFound(next);
      }
      sendResponse(doc, res);
   });

exports.getOne = (Model, popOptions) =>
   catchAsync(async (req, res, next) => {
      const queryObj = Model.findById(req.params.id);
      let doc;
      if (popOptions) {
         doc = queryObj.populate(popOptions);
      }
      doc = await doc;
      if (!doc) {
         return docNotFound(next);
      }
      sendResponse(doc, res);
   });

exports.getAll = (Model) =>
   catchAsync(async (req, res, next) => {
      const allDocs = await Model.find();
      sendResponse(allDocs, res);
   });

exports.createOne = (Model) =>
   catchAsync(async (req, res, next) => {
      const newDoc = await Model.create(req.body);
      sendResponse(newDoc, res);
   });
