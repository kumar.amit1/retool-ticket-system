const mongoose = require("mongoose");

require("./userModel");
mongoose
  .connect(process.env.DB_URL)
  .then(() => {
    console.log(`Connected to database `);
  })
  .catch((err) => {
    console.log(`Could not connect to database ${err}`);
  });

module.exports = mongoose;
