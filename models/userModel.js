const crypto = require("crypto"),
  mongoose = require("mongoose"),
  bcrypt = require("bcrypt");

const userSchema = new mongoose.Schema({
  name: {
    type: String,
    required: [true, "A user must have name, Please provide name"],
  },

  email: {
    type: String,
    unique: true,
    required: [true, "Email is required please provide the  email"],
    lowerCase: true,
  },

  password: {
    type: String,
    min: 5,
    required: [true, "Please provide the password"],
    select: false,
  },

  passwordConfirm: {
    type: String,
    min: 5,
    required: [true, "Please provide the confirm password"],
    validate: {
      validator: function (pC) {
        return pC === this.password;
      },
      message: "Passwords are not same",
    },
  },
  photo: {
    type: String,
  },

  isVerified: {
    type: Boolean,
    default: false,
  },

  role: {
    type: String,
    enum: ["user", "admin", "superuser"],
    default: "user",
  },

  departmentType: {
    type: String,
    default: "",
  },

  emailVarificationToken: String,
  emailVarificationTokenExpires: Date,
  passwordChangedAt: Date,
  passwordResetToken: String,
  passwordResetExpires: Date,
  active: {
    type: Boolean,
    default: true,
  },
});

userSchema.pre("save", async function (next) {
  if (!this.isModified("password")) return next();
  this.password = await bcrypt.hash(this.password, 11);
  this.passwordConfirm = undefined;
  next();
});

userSchema.pre("save", function (next) {
  if (!this.isModified("password") || this.isNew) return next();
  this.passwordChangedAt = Date.now();
  next();
});

userSchema.pre(/^find/, function (next) {
  this.find({ active: { $ne: false } });
  next();
});

userSchema.methods.verifyPassword = async function (
  receivedPassword,
  currentPassword
) {
  return await bcrypt.compare(receivedPassword, currentPassword);
};

userSchema.methods.isPasswordChanged = function (jwtIssuedTimeStamp) {
  if (this.passwordChangedAt) {
    const passwordChangeTimeStamp = parseInt(
      this.passwordChangedAt.getTime() / 1000,
      10
    );
    return jwtIssuedTimeStamp < passwordChangeTimeStamp;
  }
  return false;
};

userSchema.methods.createPasswordResetToken = function () {
  const resetToken = crypto.randomBytes(32).toString("hex");
  this.passwordResetToken = crypto
    .createHash("sha256")
    .update(resetToken)
    .digest("hex");
  this.passwordResetExpires = Date.now() + 10 * 60 * 1000;
  return resetToken;
};

userSchema.methods.createEmailVarificationToken = function () {
  const varificationToken = crypto.randomBytes(32).toString("hex");
  this.emailVarificationToken = crypto
    .createHash("sha256")
    .update(varificationToken)
    .digest("hex");
  this.emailVarificationTokenExpires = Date.now() + 10 * 60 * 1000;
  return varificationToken;
};

module.exports = mongoose.model("Users", userSchema);
