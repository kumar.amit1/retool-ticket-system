const express = require("express"),
  router = express.Router(),
  controllers = require("../controllers");

router.get(
  "/form/resetpassword/:resetToken",
  controllers.userControllers.getTokenFromUrl
);
router.post("/signup", controllers.userControllers.createUser);
router.get("/verifyemail/:emailToken", controllers.userControllers.verifyEmail);
router.post("/login", controllers.userControllers.loggingUser);
router.post("/forgetpassword", controllers.userControllers.forgetPassword);
router.post(
  "/resetpassword/:resetToken",
  controllers.userControllers.resetPassword
);

router.post(
  "/changepassword",
  controllers.authControllers.prevent,
  controllers.userControllers.changePassword
);

router.post(
  "/updateme",
  controllers.authControllers.prevent,
  controllers.userControllers.updateMe
);

router.get(
  "/deleteme",
  controllers.authControllers.prevent,
  controllers.userControllers.deleteUser
);

router.get(
  "/getallusers",
  controllers.authControllers.prevent,
  controllers.authControllers.restrictTo("admin"),
  controllers.userControllers.getAllUsers
);

router.get("/logout", controllers.authControllers.loggedOut);

router.delete(
  "/removeuser",
  controllers.authControllers.prevent,
  controllers.authControllers.restrictTo("admin"),
  controllers.userControllers.removeUser
);

module.exports = router;
