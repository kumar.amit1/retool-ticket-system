const dotenv = require("dotenv").config({ path: "config.env" });
const express = require("express");
const path = require("path");
const appRoutes = require("./routes");
const errorControllers = require("./controllers/errorControllers");
const helmet = require("helmet");
const rateLimit = require("express-rate-limit");
const AppError = require("./utils/appError");

const app = express();
app.use(express.urlencoded({ extended: true }));
app.use(express.static(path.join(process.cwd(), "views")));
app.set("view engine", "ejs");

// Set security HTTP headers
app.use(helmet());

app.use(express.json());

const limiter = rateLimit({
  max: 100,
  windowMs: 60 * 60 * 1000,
  message: `Too many  requests from this IP, Please try again in an hour!`,
});

// Limit request from same IP

app.use("/api", limiter);
app.use("/api/user", appRoutes.userRoutes);

app.all("*", (req, res, next) => {
  next(new AppError(`Can't find ${req.originalUrl} on this server`, 200));
});

app.use(errorControllers);

module.exports = app;
