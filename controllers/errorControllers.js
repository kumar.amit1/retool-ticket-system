const AppError = require("../utils/appError");

const sendErrDev = (err, res) => {
  err.statusCode = err.statusCode || 200;
  err.status = err.status || "error";

  res.status(err.statusCode).json({
    status: err.status,
    message: err.message,
    Error: err,
    stack: err.stack,
  });
};

const handleCasteErrorDB = (error) => {
  const message = `Invalid value ${error.value} for field ${error.path}`;
  return new AppError(message, 200);
};

const handleValidationErrorDB = (error) => {
  const value = Object.values(error.errors).map((msg) => msg.message);
  const message = value.join(" ");
  return new AppError(message, 200);
};

const handleDuplicateErrorDB = (error) => {
  const value = JSON.stringify(error).match(/[^{\}]+(?=})/g);
  const message = `Duplicate value ${value[1]}: please use another value`;
  return new AppError(message, 200);
};

const sendErrProd = (err, res) => {
  if (err.isOperational) {
    err.statusCode = err.statusCode || 200;
    err.status = err.status || "error";
    return res.status(err.statusCode).json({
      status: err.status,
      message: err.message,
    });
  }

  err.statusCode = err.statusCode || 200;

  console.log("Error", err);
  res.status(err.statusCode).json({
    message: "Something went wrong",
  });
};

module.exports = (err, req, res, next) => {
  if (process.env.NODE_ENV === "development") {
    sendErrDev(err, res);
  } else if (process.env.NODE_ENV === "production") {
    //let error = JSON.parse(JSON.stringify(err));
    // let error = { ...err };
    let error = err;
    if (error.name === "CastError") {
      error = handleCasteErrorDB(error);
    } else if (error.name === "ValidationError") {
      error = handleValidationErrorDB(error);
    } else if (error.code === 11000) {
      error = handleDuplicateErrorDB(error);
    }
    sendErrProd(error, res);
  }
};
