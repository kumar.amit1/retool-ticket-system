const userControllers = require("./userControllers"),
  authControllers = require("./authControllers");

module.exports = {
  userControllers,
  authControllers,
};
