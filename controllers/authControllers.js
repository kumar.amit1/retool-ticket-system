const catchAsync = require("../utils/catchAsync"),
  AppError = require("../utils/appError"),
  util = require("util"),
  jwt = require("jsonwebtoken"),
  mongoose = require("../models"),
  userSchema = mongoose.models.Users;

// Protect the route from unauthorised access
exports.prevent = catchAsync(async (req, res, next) => {
  const { authorization } = req.headers;
  let token;
  if (authorization && authorization.startsWith("Bearer")) {
    token = authorization.split(" ")[1];
  }
  if (!token) {
    return next(new AppError("Please login to get access"));
  }

  const verifyToken = util.promisify(jwt.verify);
  const decodedToken = await verifyToken(token, process.env.JWT_SECRET);

  const currentUser = await userSchema.findById(decodedToken.id);

  if (!currentUser) {
    return next(new AppError("No user found with the token ", 200));
  }

  if (currentUser.isPasswordChanged(decodedToken.iat)) {
    return next(
      new AppError("Password was changed recently, Please login again")
    );
  }

  req.user = currentUser;
  next();
});

exports.restrictTo = (...roles) => {
  return function (req, res, next) {
    const user = req.user;
    if (!roles.includes(user.role)) {
      return next(
        new AppError("You are not authorized to perform this action")
      );
    }
    next();
  };
};

// To check if user is logged in or not
exports.isLoggedIn = async (req, res, next) => {
  try {
    if (req.cookies.jwt) {
      const verifyToken = util.promisify(jwt.verify);
      const decodedToken = await verifyToken(
        req.cookies.jwt,
        process.env.JWT_SECRET
      );
      const currentUser = await userSchema.findById(decodedToken.id);
      if (!currentUser) {
        res.locals.user = "";
        return next();
      }
      res.locals.user = currentUser;
      return next();
    }
    res.locals.user = "";
  } catch (err) {
    res.locals.user = "";
    return next();
  }
  next();
};

// This controller function handle the api request to serve the purpose of logging out a user
exports.loggedOut = async (req, res, next) => {
  res.cookie("jwt", "loggedout", {
    expires: new Date(Date.now() + 10 * 1000),
    http: true,
  });
  res.status(200).json({
    status: "success",
  });
};
