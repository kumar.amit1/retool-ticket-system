const jwt = require("jsonwebtoken"),
  crypto = require("crypto"),
  sendEmail = require("../utils/email"),
  mongoose = require("../models"),
  userSchema = mongoose.models.Users,
  catchAsync = require("../utils/catchAsync"),
  AppError = require("../utils/appError"),
  factory = require("../services/factoryHandler");

const createToken = (id) => {
  return jwt.sign({ id }, process.env.JWT_SECRET, {
    expiresIn: "5d",
  });
};

const sendResponseWithCookie = (user, statusCode, res) => {
  const token = createToken(user._id);
  const cookieOptions = {
    expiresIn: new Date(Date.now() + 90 * 24 * 60 * 60 * 1000),
    httpOnly: true,
  };
  user.password = undefined;
  user.active = undefined;
  user.isVerified = undefined;
  user.emailVarificationTokenExpire = undefined;
  res.cookie("jwt", token, cookieOptions);
  res.status(statusCode).json({
    status: "success",
    data: {
      user,
      token,
    },
  });
};

const filterBody = function (filterObj, ...data) {
  let obj = {};
  Object.keys(filterObj).forEach((x) => {
    if (data.includes(x)) {
      obj[x] = filterObj[x];
    }
  });
  return obj;
};

exports.createUser = catchAsync(async (req, res, next) => {
  const { name, email, password, passwordConfirm } = req.body;

  const checkUser = await userSchema.findOne({ email });
  if (checkUser) return next(new AppError("User already exist", 200));

  const newUser = await userSchema.create({
    name: name,
    email: email,
    password: password,
    passwordConfirm: passwordConfirm,
  });

  const emailVarificationToken = newUser.createEmailVarificationToken();
  await newUser.save({ validateBeforeSave: false });
  // const verificationToken = `${req.protocol}://${req.hostname}:${req.socket.localPort}/api/user/verifyemail/${emailVarificationToken} \n click the link to verify email`;
  const verificationToken = `${req.protocol}://${req.hostname}/api/user/verifyemail/${emailVarificationToken} \n click the link to verify email`;
  const options = {
    to: newUser.email,
    subject: "Email verification link",
    body: verificationToken,
  };

  await sendEmail(options);
  res.status(200).json({
    status: "success",
    message: "Verification email sent..Please verify the email",
  });
});

// Function to verify the email
exports.verifyEmail = catchAsync(async (req, res, next) => {
  const emailToken = req.params.emailToken;

  if (!emailToken) {
    return next(new AppError("No  token found, Please try again", 200));
  }

  const hashedEmailToken = crypto
    .createHash("sha256")
    .update(emailToken)
    .digest("hex");
  const user = await userSchema.findOne({
    emailVarificationToken: hashedEmailToken,
    emailVarificationTokenExpires: { $gt: Date.now() },
  });
  if (!user) {
    return next(
      new AppError(
        "The link has expired or token is invalid, Please try again",
        200
      )
    );
  }

  user.isVerified = true;
  user.emailVarificationToken = undefined;
  user.emailVarificationTokenExpires = undefined;
  await user.save({ validateBeforeSave: false });

  // res.status(200).json({
  //   status: "success",
  //   data: "User verified successfully...Please login",
  // });
  res
    .status(200)
    .send(
      '<h2 align="center" style="color: green;">Email verified successfully...Please go to login</h2>'
    );
});

// function to loggin a user
exports.loggingUser = catchAsync(async (req, res, next) => {
  const { email, password } = req.body;

  if (!email || !password) {
    return next(new AppError("Please provide email and password", 200));
  }

  const user = await userSchema.findOne({ email }).select("+password");
  // if (!user) {
  //   return next(new AppError("This user does not exist", 404));
  // }

  if (!user || !(await user.verifyPassword(password, user.password))) {
    return next(new AppError("Please provide correct email or password", 200));
  }
  if (!user.isVerified) {
    return next(new AppError("Please verify your email", 200));
  }

  sendResponseWithCookie(user, 200, res);
});

exports.forgetPassword = catchAsync(async (req, res, next) => {
  const { email } = req.body;
  const user = await userSchema.findOne({ email });
  if (!user) {
    return next(new AppError("No user exist with this email", 200));
  }

  const resetToken = user.createPasswordResetToken();
  await user.save({ validateBeforeSave: false });

  // const message = `${req.protocol}://${req.hostname}:${req.socket.remotePort}/api/user/resetPassword/${resetToken} \n click the link to reset the password`;

  const message = `${req.protocol}://${req.hostname}/api/user/form/resetPassword/${resetToken} \n click the link to reset the password`;

  const options = {
    to: email,
    subject: "Reset password link",
    body: message,
  };

  try {
    await sendEmail(options);
    return res.status(200).json({
      status: "success",
      message: `Email sent to ${options.to}`,
    });
  } catch (err) {
    user.passwordResetToken = undefined;
    user.passwordResetExpires = undefined;
    await user.save({ validateBeforeSave: false });
    return next(
      new AppError("Something wrong happend while sending mail", 200)
    );
  }
});

exports.resetPassword = catchAsync(async (req, res, next) => {
  const resetToken = req.params.resetToken;

  if (!resetToken) {
    return next(new AppError("No reset token found, Please try again", 200));
  }

  const hashedToken = crypto
    .createHash("sha256")
    .update(resetToken)
    .digest("hex");
  const user = await userSchema.findOne({
    passwordResetToken: hashedToken,
    passwordResetExpires: { $gt: Date.now() },
  });
  if (!user) {
    return next(
      new AppError(
        "The link has expired or token is invalid, Please try again",
        200
      )
    );
  }

  user.password = req.body.password;
  user.passwordConfirm = req.body.passwordConfirm;
  user.passwordResetToken = undefined;
  user.passwordResetExpires = undefined;
  await user.save();

  // res.status(200).json({
  //   status: "success",
  //   data: "Password updated successfully",
  // });
  res
    .status(200)
    .send(
      '<h3 align="center" style="color: green;">Password has been reset successfully...Please go to login</h3>'
    );
});

exports.changePassword = catchAsync(async (req, res, next) => {
  const user = req.user;
  const { currentPassword, password, passwordConfirm } = req.body;

  if (!currentPassword || !password || !passwordConfirm) {
    return next(new AppError("Please provide the passwords", 200));
  }

  const currentUser = await userSchema.findById(user.id).select("+password");

  if (
    !currentUser ||
    !(await currentUser.verifyPassword(currentPassword, currentUser.password))
  ) {
    return next(
      new AppError("Current password is not correct, Please try again")
    );
  }

  currentUser.password = req.body.password;
  currentUser.passwordConfirm = req.body.passwordConfirm;
  await currentUser.save();

  res.status(200).json({
    status: "success",
    message: "Password updated successfully",
  });
  // res.status(200).send("Password has been reset successfully...Please go to  login")
});

exports.deleteUser = catchAsync(async (req, res, next) => {
  await userSchema.findByIdAndUpdate(req.user.id, { active: false });

  res.status(200).json({
    status: "success",
    message: "User account deativated successfully",
  });
});

exports.updateMe = catchAsync(async (req, res, next) => {
  if (req.body.password || req.body.passwordConfirm) {
    return next(
      new AppError("Please visit /changePassword, to update the password")
    );
  }

  const currentUser = userSchema.findById(req.user.id);
  if (!currentUser) {
    return next(new AppError("Please login again to update ", 200));
  }

  const filter = filterBody(req.body, "name", "email");

  const updatedUser = userSchema.findByIdAndUpdate(currentUser._id, filter, {
    new: true,
    runValidators: true,
  });
  res.status(200).json({
    status: "success",
    data: {
      data: updatedUser,
    },
  });
});

exports.getAllUsers = catchAsync(async (req, res, next) => {
  const allUsers = await userSchema.find();

  res.status(200).json({
    status: "success",
    result: allUsers.length,
    data: {
      users: allUsers,
    },
  });
});

exports.getTokenFromUrl = catchAsync(async (req, res, next) => {
  const token = req.params.resetToken;
  res.status(200).render("passwordReset", { token: token });
});

exports.removeUser = factory.deleteOne(userSchema);
